﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    [SerializeField] protected float _velocity = 20f;
    [SerializeField] protected float _damage = 5f;
    [SerializeField] protected float _lifeTime = 2f;
    [SerializeField] protected int _team = 0;

    protected Rigidbody2D _rigidbody;

    protected virtual void Start()
    {
        // set initial velocity and start lifetime
        _rigidbody = GetComponent<Rigidbody2D>();
        _rigidbody.velocity = transform.right * _velocity;
        Destroy(gameObject, _lifeTime);
    }

    protected virtual void OnTriggerEnter2D(Collider2D other)
    {
        // check for valid target and deal damage
        Health possibleEnemy = other.GetComponent<Health>();
        if(possibleEnemy != null)
        {
            if(possibleEnemy.Team == _team) // projectile and target are same team
            {
                return;
            }
            else    // projectile and target are enemies
            {
                possibleEnemy.Damage(_damage);
            }
        }

        // destroy projectile
        Destroy(gameObject);
    }
}

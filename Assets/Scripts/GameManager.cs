﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    public int Score {get; private set;}

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
            Score = 0;
        }
        else
        {
            Destroy(this);
        }
    }

    public void ChangeScore(int scoreValue)
    {
        Score += scoreValue;
    }



}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChaseEnemy : Unit
{
    [SerializeField] private float _collisionDamage = 5f;

    private Health _target;

    private void OnTriggerEnter2D(Collider2D other)
    {
        // assign target if valid health with opposing team enters trigger
        Health possibleTarget = other.GetComponent<Health>();
        if(possibleTarget != null && possibleTarget.Team != _health.Team)
        {
            _target = possibleTarget;
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        // damage other if it has a valid health component and is an enemy
        Health possibleEnemy = other.gameObject.GetComponent<Health>();
        if(possibleEnemy != null && possibleEnemy.Team != _health.Team)
        {
            possibleEnemy.Damage(_collisionDamage);
        }
    }

    protected override void Update()
    {
        base.Update();

        float moveInput = 0f;

        // alive with valid target
        if(_health.IsAlive && _target != null)
        {
            // assign move input based on direction to target
            float distanceToPlayer = _target.transform.position.x - transform.position.x;
            moveInput = Mathf.Clamp(distanceToPlayer, -1f, 1f);
        }

        // apply move input
        _characterController.SetMoveInput(moveInput);
    }
}
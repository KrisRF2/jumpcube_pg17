﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterController2D : MonoBehaviour
{
    [Header("Movement")]
    [SerializeField] private float _speed = 5f;
    [SerializeField] private float _acceleration = 10f;
    [SerializeField] private bool _canWalkOffLedges = false;

    [Header("Jump")]
    [SerializeField] private float _jumpVelocity = 5f;
    [SerializeField] private float _airControl = 0.1f;

    [Header("Grounding")]
    [SerializeField] private float _groundCheckDistance = 0.25f;
    [SerializeField] private float _groundCheckOffset = 0.35f;
    [SerializeField] private LayerMask _groundCheckMask;

    // unity components
    private Rigidbody2D _rigidbody;
    private Animator _animator;
    private SpriteRenderer _spriteRenderer;

    // properties
    public bool IsGrounded {get; private set;}
    public bool IsTryingToMove { get; private set; }
    public bool IsMovingRight {get; private set;} = true;

    // movement values
    private float _moveInput;

    private void Start()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
        _animator = GetComponent<Animator>();
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }

    // set left-right movement input
    public void SetMoveInput(float input)
    {
        if(!_canWalkOffLedges)
        {
            // set input to 0 if going to walk off ledge
            if(input > 0f && !GroundCheck(_groundCheckOffset) ||
               input < 0f && !GroundCheck(-_groundCheckOffset))
               {
                   input = 0f;
               }
        }

        _moveInput = input;
        IsTryingToMove = Mathf.Abs(_moveInput) >= 0.05f;

        if(IsTryingToMove)
        {
            IsMovingRight = _moveInput > 0f;
        }
    }

    public void TryJump()
    {
        // return early if not grounded
        if(!IsGrounded) return;

        // override vertical velocity
        Vector2 currentVelocity = _rigidbody.velocity;
        currentVelocity.y = _jumpVelocity;
        _rigidbody.velocity = currentVelocity;

        // * set jump trigger in animator
        _animator.SetTrigger("Jump");
    }

    private void Update()
    {
        bool grounded = GroundCheck(-_groundCheckOffset) || GroundCheck(_groundCheckOffset);
        IsGrounded = grounded;

        // * send values to animator
        _animator.SetFloat("Speed", Mathf.Abs(_moveInput));
        _animator.SetBool("IsGrounded", IsGrounded);

        // * set sprite direction
        if(IsTryingToMove && IsGrounded)
        {
            _spriteRenderer.flipX = _moveInput < 0f;
        }
    }

    private void FixedUpdate()
    {
        // * find control value
        float control = 0f;                             // airborne, not trying to move
        if(IsGrounded) control = 1f;                    // full control on ground
        else if(IsTryingToMove) control = _airControl;  // partial control while trying to move in air

        // * calculate target, current velocity, and apply acceleration
        float targetVelocity = _moveInput * _speed;
        float currentVelocity = _rigidbody.velocity.x;
        float velocityDifference = targetVelocity - currentVelocity;
        float moveForce = velocityDifference * _acceleration * control;
        _rigidbody.AddForce(new Vector2(moveForce, 0f));
    }

    private bool GroundCheck(float horizontalOffset)
    {
        // create origin position
        Vector2 offset = new Vector2(horizontalOffset, _groundCheckDistance * 0.5f);
        Vector2 origin = offset + (Vector2)transform.position;

        // check for ground using origin position
        if(Physics2D.Raycast(origin, Vector2.down, _groundCheckDistance, _groundCheckMask))
        {
            // ground found
            Debug.DrawRay(origin, Vector2.down * _groundCheckDistance, Color.green);
            return true;
        }
        else
        {
            // no ground
            Debug.DrawRay(origin, Vector2.down * _groundCheckDistance, Color.red);
            return false;
        }
    }
}

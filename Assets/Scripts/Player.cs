﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Unit
{
    [Header("Inputs")]
    [SerializeField] private string _moveInput = "Horizontal";
    [SerializeField] private string _jumpInput = "Jump";
    [SerializeField] private string _fireInput = "Fire";
    [SerializeField] private string _altFireInput = "AltFire";

    [Header("Weapons")]
    [SerializeField] private Weapon _primaryWeapon;
    [SerializeField] private Weapon _secondaryWeapon;
    

    protected override void Update()
    {
        base.Update();

        // apply move input from player to character controller
        float input = _health.IsAlive ? Input.GetAxis(_moveInput) : 0f;
        _characterController.SetMoveInput(input);

        if(!_health.IsAlive) return;

        // jump
        if(Input.GetButtonDown(_jumpInput)) _characterController.TryJump();

        // fire primary weapon
        if(Input.GetButton(_fireInput)) _primaryWeapon.TryFire();

        // fire secondary weapon
        if(Input.GetButton(_altFireInput)) _secondaryWeapon.TryFire();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit : MonoBehaviour
{
    protected CharacterController2D _characterController;
    protected Health _health;
    protected Animator _animator;

    private void Start()
    {
        _characterController = GetComponent<CharacterController2D>();
        _health = GetComponent<Health>();
        _animator = GetComponent<Animator>();

        // register Death to Health component's death event
        _health.DeathEvent.AddListener(Death);
    }

    protected virtual void Update()
    {
        // set animator alive
        _animator.SetBool("IsAlive", _health.IsAlive);

        // stop movement and return early if dead
        if(!_health.IsAlive)
        {
            _characterController.SetMoveInput(0f);
            return;
        }
    }

    protected virtual void Death()
    {
        // change to corpse layer
        gameObject.layer = LayerMask.NameToLayer("Corpse");
    }
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    // muzzles to spawn projectile from
    [Header("Spawning")]
    [SerializeField] protected Transform _muzzleRight;
    [SerializeField] protected Transform _muzzleLeft;
    [SerializeField] protected Projectile _projectilePrefab;

    [Header("Balance")]
    [SerializeField] protected float _fireRate = 120f;

    public float FireCooldown => 60f / _fireRate;
    private float _nextFireTime;
    private CharacterController2D _characterController;

    private void Start()
    {
        // get CharacterController2D from parent gameobject
        _characterController = transform.parent.GetComponent<CharacterController2D>();
    }

    public void TryFire()
    {
        // check if we can fire again
        if(Time.timeSinceLevelLoad >= _nextFireTime)
        {
            // set next fire time at some point in the future
            _nextFireTime = Time.timeSinceLevelLoad + FireCooldown;

            Fire(_characterController.IsMovingRight);
        }
    }

    protected virtual void Fire(bool isMovingRight)
    {
        // find the correct muzzle
        Transform muzzle = isMovingRight ? _muzzleRight : _muzzleLeft;
        // get position of muzzle
        Vector3 spawnPosition = muzzle.position;
        // get rotation of muzzle
        Quaternion spawnRotation = muzzle.rotation;
        // instantiate the projectile
        Instantiate(_projectilePrefab, spawnPosition, spawnRotation);
    }
}

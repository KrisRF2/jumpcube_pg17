﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    // the thing to follow
    [SerializeField] private Transform _target;
    // height and distance from target for camera
    [SerializeField] private float _fixedHeight = 10f;
    [SerializeField] private float _fixedDepth = -30f;
    // camera move speed
    [SerializeField] private float _lerpSpeed = 5f;

    private void Update()
    {
        if(_target == null) return;

        // desired position for camera
        Vector3 targetPos = new Vector3(_target.position.x, _fixedHeight, _fixedDepth);
        // lerp to target position
        transform.position = Vector3.Lerp(transform.position, targetPos, Time.deltaTime * _lerpSpeed);
    }
}
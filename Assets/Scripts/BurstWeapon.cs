﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BurstWeapon : Weapon
{
    // number of shots fire in burst
    [SerializeField] private int _shotCount = 3;
    // delay between shots in burst
    [SerializeField] private float _shotDelay = 0.1f;

    // override base Weapon.Fire function
    protected override void Fire(bool isMovingRight)
    {
        // start FireRoutine Coroutine
        StartCoroutine(FireRoutine(isMovingRight));
    }

    // fire coroutine
    private IEnumerator FireRoutine(bool isMovingRight)
    {
        for (int i = 0; i < _shotCount; i++)
        {
            base.Fire(isMovingRight);
            
            // add delay before next iteration of for loop
            yield return new WaitForSeconds(_shotDelay);
        }
    }
}